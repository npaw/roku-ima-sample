Library "Roku_Ads.brs"
  Library "IMA3.brs"
  sub init()
    m.top.functionName = "runThread"
    m.top.seekStartTime = -1
    m.top.seekEndTime = -1
    m.top.snapbackTime = -1
    m.top.adPlaying = False

    m.youboraPlugin = CreateObject("roSGNode", "YBPluginRokuVideo")
  End Sub
  sub runThread()
    if not m.top.sdkLoaded
      loadSdk()
    End If
    if not m.top.streamManagerReady
      loadStream()
    End If
    If m.top.streamManagerReady
      runLoop()
    End If
  End Sub
  Sub runLoop()
    ' Forward all timed metadata events.
    m.top.video.timedMetaDataSelectionKeys = ["*"]
    ' Cycle through all the fields and just listen to them all.
    m.port = CreateObject("roMessagePort")
    fields = m.top.video.getFields()
    for each field in fields
      m.top.video.observeField(field, m.port)
    end for
    m.lastLoopTime = m.top.video.position
    m.seekThreshold = 2
    interactivePlayer = {
      sgNode: m.top.video,
      port: m.port
    }
    adIface = Roku_Ads() ' used to enable interactive ads
    setupLogObject(adIface)
    while True
      msg = wait(1000, m.port)
      if m.top.video = invalid
        print "exiting"
        exit while
      end if
      curAd = adIface.stitchedAdHandledEvent(msg, interactivePlayer)
      If curAd <> Invalid and curAd.evtHandled = true and curAd.adExited = true
        print "Interactive ad requesting exiting"
        exit while
      End If
      m.streamManager.onMessage(msg)
      m.top.contentTime = m.streamManager.getContentTime(m.top.video.position * 1000)/1000
      currentTime = m.top.video.position
      If currentTime > 3 And not m.top.adPlaying
         m.top.video.enableTrickPlay = true
      End If
      if abs(currentTime - m.lastLoopTime) > m.seekThreshold
        print "Seek detected from "; m.lastLoopTime;" to "currentTime
        if m.top.inSnapback
          ' That seek was us snapping back to content
          print "Threshold break was snapback"
          m.top.inSnapback = false
        else
          ' User seeked
          print "Threshold break was user seek - sending to onUserSeek"
          onUserSeek(m.lastLoopTime, currentTime)
        end if
      end if
      m.lastLoopTime = currentTime
    end while
  End Sub
  sub loadSdk()
    If m.sdk = invalid
      m.sdk = New_IMASDK()
    End If
    m.top.sdkLoaded = true
  End Sub
  sub setupVideoPlayer()
    sdk = m.sdk
    setupYoubora()
    m.player = sdk.createPlayer()
    m.player.top = m.top
    m.player.loadUrl = Function(urlData)
      if m.top.streamData.type <> "live" then
        bookmarkTime = m.top.streamData.bookmarkTime*1000
        m.top.bookmarkStreamTime = m.streamManager.getStreamTime(bookmarkTime) / 1000
      else
        m.top.bookmarkStreamTime = 0
      endif
      ' This line prevents users from scanning during buffering or during the first second of the
      ' ad before we have a callback from roku.
      ' If there are no prerolls disabling trickplay isn't needed.
      m.top.video.enableTrickPlay = false
      m.top.urlData = urlData
    End Function
    m.player.adBreakStarted = Function(adBreakInfo as Object)
      print "---- Ad Break Started ---- ";adBreakInfo
      m.top.adPlaying = True
      m.top.video.enableTrickPlay = false
      processImaAdEvent("PodStart", adBreakInfo)
    End Function
    m.player.adBreakEnded = Function(adBreakInfo as Object)
      print "---- Ad Break Ended ---- ";adBreakInfo
      m.top.adPlaying = False
      if m.top.snapbackTime > -1 and m.top.snapbackTime > m.top.video.position
        m.top.video.seek = m.top.snapbackTime
        m.top.snapbackTime = -1
      end if
      m.top.video.enableTrickPlay = true
      processImaAdEvent("PodComplete", adBreakInfo)
    End Function
  End Sub
  sub onUserSeek(seekStartTime as Integer, seekEndTime as Integer)
    previousCuePoint = m.streamManager.getPreviousCuePoint(seekEndTime)
    if previousCuePoint = invalid
      print "Previous cuepoint was invalid"
      return
    else if previousCuePoint.hasPlayed
      print "Previous cuepoint was ";previousCuepoint.start
      print "Previous cuepoint was played"
      return
    else
      print "Previous cuepoint was ";previousCuepoint.start
      ' Add a second to make sure we hit the keyframe at the start of the ad
      print "Seeking to ";previousCuepoint.start+1
      m.top.video.seek = previousCuePoint.start + 1
      m.top.snapbackTime = seekEndTime
      m.top.inSnapback = true
    end if
  end sub
  Sub loadStream()
    sdk = m.sdk
    sdk.initSdk()
    setupVideoPlayer()
    request = sdk.CreateStreamRequest()
    print "Bookmark time = ";m.top.streamData.bookmarkTime
    if m.top.streamData.type = "live"
      request.assetKey = m.top.streamData.assetKey
    else
      request.contentSourceId = m.top.streamData.contentSourceId
      request.videoId = m.top.streamData.videoId
    end if
    request.apiKey = m.top.streamData.apiKey
    request.player = m.player
    requestResult = sdk.requestStream(request)
    If requestResult <> Invalid
      print "Error requesting stream ";requestResult
    Else
      m.streamManager = Invalid
      While m.streamManager = Invalid
        sleep(50)
        m.streamManager = sdk.getStreamManager()
      End While
      If m.streamManager = Invalid or m.streamManager["type"] <> Invalid or m.streamManager["type"] = "error"
        errors = CreateObject("roArray", 1, True)
        print "error ";m.streamManager["info"]
        errors.push(m.streamManager["info"])
        m.top.errors = errors
      Else
        m.top.streamManagerReady = True
        addCallbacks()
        m.player.streamManager = m.streamManager
        m.streamManager.start()
      End If
    End If
  End Sub
  Function addCallbacks() as Void
    m.streamManager.addEventListener(m.sdk.AdEvent.ERROR, errorCallback)
    m.streamManager.addEventListener(m.sdk.AdEvent.AD_PERIOD_STARTED, periodStartCallback)
    m.streamManager.addEventListener(m.sdk.AdEvent.START, startCallback)
    m.streamManager.addEventListener(m.sdk.AdEvent.PROGRESS, progressCallback)
    m.streamManager.addEventListener(m.sdk.AdEvent.FIRST_QUARTILE, firstQuartileCallback)
    m.streamManager.addEventListener(m.sdk.AdEvent.MIDPOINT, midpointCallback)
    m.streamManager.addEventListener(m.sdk.AdEvent.THIRD_QUARTILE, thirdQuartileCallback)
    m.streamManager.addEventListener(m.sdk.AdEvent.COMPLETE, completeCallback)
    m.streamManager.addEventListener(m.sdk.AdEvent.AD_PERIOD_ENDED, periodEndCallback)
  End Function
  Function periodStartCallback(ad as Object) as Void
    'processImaAdEvent(m.sdk.AdEvent.AD_PERIOD_STARTED, ad)
  End Function
  Function startCallback(ad as Object) as Void
  processImaAdEvent(m.sdk.AdEvent.START, ad)
    print "Callback from SDK -- Start called - "; ad
    ' Allows raf control in case of interactive ads
    If ad.companions <> Invalid And ad.companions.count() > 0
      rafStructure = convertToRaf(ad, m.top.video.position)
      logStructure(rafStructure, "") ' Uncomment to see RAF structure
      adIface = Roku_Ads()
      adIface.stitchedAdsInit(rafStructure)
    End If
  End Function
  Function progressCallback(ad as Object) as Void
    processImaAdEvent(m.sdk.AdEvent.PROGRESS, ad)
  End Function
  Function firstQuartileCallback(ad as Object) as Void
    processImaAdEvent(m.sdk.AdEvent.FIRST_QUARTILE, ad)
    print "Callback from SDK -- First quartile called - "
  End Function
  Function midpointCallback(ad as Object) as Void
    processImaAdEvent(m.sdk.AdEvent.MIDPOINT, ad)
    print "Callback from SDK -- Midpoint called - "
  End Function
  Function thirdQuartileCallback(ad as Object) as Void
    processImaAdEvent(m.sdk.AdEvent.THIRD_QUARTILE, ad)
    print "Callback from SDK -- Third quartile called - "
  End Function
  Function completeCallback(ad as Object) as Void
    processImaAdEvent(m.sdk.AdEvent.COMPLETE, ad)
    print "Callback from SDK -- Complete called - "
  End Function
  Function periodEndCallback(ad as Object) as Void
    'processImaAdEvent(m.sdk.AdEvent.AD_PERIOD_ENDED, ad)
    print "Callback from SDK -- Period end called - "
  End Function
  Function errorCallback(error as Object) as Void
    processImaAdEvent(m.sdk.AdEvent.ERROR, ad)
    print "Callback from SDK -- Error called - "; error
    ' errors are critical and should terminate the stream.
    m.errorState = True
  End Function


sub setupYoubora() 

youboracfg = FormatJson({
    "accountCode": "powerdev", 'Change for you accountcode
    "expectAds": "true",
    "anonymousUser": "hello!",
    "content.title": "Sintel",
    "content.metadata": {
        "year": "2001",
        "genre": "Fantasy",
        "price": "free"
    },
    "username": "holi1",
    "user.name": "holi3",
    "ad.title": "adtitle",
    "network.ip": "1.2.3.4",
    "app.name": "asd",
    "app.releaseVersion": "2.2",
    "content.customDimension.1": "Extra param 1 value",
    "content.customDimension.12": "Extra param 12 value",
    "ad.customDimension.7": "Extra param for ad 7",
    "user.email": "1",
    "content.package" : "2",
    "content.saga" : "3",
    "content.tvShow" : "4",
    "content.season" : "5",
    "content.episodeTitle" : "6",
    "content.Channel" : "7",
    "content.id" : "8",
    "content.imdbId" : "9",
    "content.gracenoteId" : "10",
    "content.type" : "11",
    "content.genre" : "12",
    "content.language" : "13",
    "content.subtitles" : "14",
    "content.contractedResolution" : "15",
    "content.cost" : "16",
    "content.price" : "17",
    "content.playbackType" : "18",
    "content.drm" : "19",
    "content.encoding.videoCodec" : "20",
    "content.encoding.audioCodec" : "21",
    "content.encoding.codecSettings" : "22",
    "content.encoding.codecProfile" : "23",
    "content.encoding.containerFormat" : "24"
    "content.metrics" : {
        "key" : "value"
    }
})

m.youboraPlugin.videoplayer = m.top.video
m.youboraPlugin.options = ParseJson(youboracfg)
m.youboraPlugin.control = "RUN" 'Start monitoring
end sub

function setupLogObject(adIface as object)
' Create a log object to track events
logObj = {
    Log: function(evtType = invalid as dynamic, ctx = invalid as dynamic)
        if GetInterface(evtType, "ifString") <> invalid
            ? "*** tracking event " + evtType + " fired."
            if ctx.errMsg <> invalid then ? "*****   Error message: " + ctx.errMsg
            if ctx.adIndex <> invalid then ? "*****  Ad Index: " + ctx.adIndex.ToStr()
            if ctx.ad <> invalid and ctx.ad.adTitle <> invalid then ? "*****  Ad Title: " + ctx.ad.adTitle
            'else if ctx <> invalid and ctx.time <> invalid
            '? "*** checking tracking events for ad progress: " + ctx.time.ToStr()
        end if
    end function
}
' Create a log function to track events
logFunc = function(obj = invalid as dynamic, evtType = invalid as dynamic, ctx = invalid as dynamic)
    obj.log(evtType, ctx)
end function
' Setup tracking events callback
setLog = adIface.SetTrackingCallback(logFunc, logObj)
end function

function processImaAdEvent(event as string, ad as object)
    imaEvent = {
        "event": event,
        "ad": ad
    }
    m.youboraPlugin.imaadevent = ParseJson(FormatJson(imaEvent))
end function